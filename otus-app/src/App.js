import logo from './logo.svg';
import './App.css';
import AddRole from './Componens/AddRole';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
              <AddRole/>
      </header>
    </div>
  );
}

export default App;
