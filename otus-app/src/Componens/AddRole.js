import React, { useState } from 'react';
import axios from 'axios'

export default function AddRole(props) {
    const [role, setRole] = React.useState('');
    const [description, setDescription] = React.useState('');

    function RoleSubmit() {
        axios.post('https://localhost:5001/api/v1/Roles', { role: role, description: description });
            
    };

    return (
        <form onSubmit={RoleSubmit}>
            <div>
                <label htmlFor='role'>Role: </label>
                <input type='text' id='role' value={role} onChange={(e) => setRole(e.target.value)} />
            </div>
            <div>
                <label htmlFor='description'>Description: </label>
                <input type='text' id='description' value={description} onChange={(e) => setDescription(e.target.value)} />
            </div>
            <div>
                <label htmlFor='send'>Send: </label>
                <button id='send'>Send</button>
            </div>
        </form>
    );
}